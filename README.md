# Test pour Adimeo

Bonjour, je suis Dimitri Bahuaud et j'ai réalisé un test pour Adimeo,
pour le poste d'intégrateur HTML/CSS.

# Un exemple ?
Après le test, j'ai essayé de chercher le site sur Internet, pour voir les solutions choisies pour ce site (https://www.mdig.fr/). J'ai été curieux, notamment sur le footer en responsive, je n'ai pas pensé à re répartir les éléments de cette manière.

# Les difficultés
## Le responsive
Étant complètement libre sur le responsive, j'ai construit le site en desktop-first.
De fait, sur le réarrangement des blocs, je pense qu'il y a de meilleures façons d'optimiser les espaces, notamment sur le "Soutenez le musée" et le footer.

## Les typographies
Je n'avais pas compris que "Museo" et "Museo-Sans" étaient 2 typos différentes.
J'ai donc créé une autre class pour les titres et éléments qui devaient en bénéficier.

## .DS_STORE
Bien que ce ne soit pas forcément une difficulté, mon ordinateur personnel est un Mac.
Je travaille sur git, avec un .git_ignore, mais.. Il se peut qu'il en reste à l'intérieur du projet.
